﻿namespace Pankki
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saldolabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.talleta = new System.Windows.Forms.Button();
            this.nosta = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // saldolabel
            // 
            this.saldolabel.AutoSize = true;
            this.saldolabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.saldolabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saldolabel.Location = new System.Drawing.Point(335, 74);
            this.saldolabel.MinimumSize = new System.Drawing.Size(150, 0);
            this.saldolabel.Name = "saldolabel";
            this.saldolabel.Size = new System.Drawing.Size(150, 31);
            this.saldolabel.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(239, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Saldo:";
            // 
            // talleta
            // 
            this.talleta.Location = new System.Drawing.Point(335, 189);
            this.talleta.Name = "talleta";
            this.talleta.Size = new System.Drawing.Size(75, 23);
            this.talleta.TabIndex = 2;
            this.talleta.Text = "Talleta";
            this.talleta.UseVisualStyleBackColor = true;
            // 
            // nosta
            // 
            this.nosta.Location = new System.Drawing.Point(335, 282);
            this.nosta.Name = "nosta";
            this.nosta.Size = new System.Drawing.Size(75, 23);
            this.nosta.TabIndex = 3;
            this.nosta.Text = "Nosta";
            this.nosta.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 401);
            this.Controls.Add(this.nosta);
            this.Controls.Add(this.talleta);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.saldolabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label saldolabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button talleta;
        private System.Windows.Forms.Button nosta;
    }
}

