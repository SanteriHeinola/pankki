﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pankki
{
    public partial class Form1 : Form
    {
        string saldo = "";
        string fileName = AppDomain.CurrentDomain.BaseDirectory + "C:\temp/saldo.txt";
        talletusLomake tl = new Pankki.talletusLomake();
        nostolomake nl;


        public Form1()
        {
            InitializeComponent();
            nl = new nostolomake(this);
            lueSaldo();
            talleta.Click += talletusLomake;
            nosta.Click += nostoLomake;
        }

        public void paivitaSaldo(string s)
        {
            saldo = s;
            saldolabel.Text = saldo;

        }
        private void nostoLomake(object sender, EventArgs e)
        {
            nl.setSaldo(saldo);
            nl.Show();
        }

        private void talletusLomake(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void lueSaldo()
        {
            if (!File.Exists(fileName))
            {
                alustaSaldoTiedosto();
            }
            using (StreamReader sr = new StreamReader(fileName))
            {
                // Read the stream to a string, and write the string to the console.
                saldo = sr.ReadToEnd();
            }
            saldolabel.Text = saldo;
        }

        private void alustaSaldoTiedosto()
        {
            // Write the string array to a new file named "WriteLines.txt".
            using (StreamWriter outputFile = new StreamWriter(fileName))
            {
                outputFile.WriteLine("1000");
            }
        }
    }
}
